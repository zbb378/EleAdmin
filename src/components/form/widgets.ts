import {
  ElCascader,
  ElCheckboxGroup,
  ElColorPicker,
  ElDatePicker,
  ElInput,
  ElRadioGroup,
  ElRate,
  ElSelect,
  ElSlider,
  ElSwitch,
  ElTimePicker
} from 'element-plus';

import FileButton from './widget/FileWidget.vue';
import EpxPair from './widget/PairWidget.vue';
import EpxRelation from './widget/RelationWidget.vue';
import WhenWidget from './widget/WhenWidget.vue';
import OptionsWidget from './widget/OptionsWidget.vue';

const Widgets = {
  input: ElInput,
  text: ElInput,

  switch: ElSwitch,

  radio: ElRadioGroup,
  checkbox: ElCheckboxGroup,
  select: ElSelect,
  cascader: ElCascader,

  number: ElSlider,

  date: ElDatePicker,
  time: ElTimePicker,

  color: ElColorPicker,
  rate: ElRate,

  file: FileButton,
  pair: EpxPair,

  relation: EpxRelation
};

export type WidgetName = keyof typeof Widgets;
export type InnerWidget = 'when' | 'option';

export const getWidget = (name: WidgetName | InnerWidget) => {
  if (name === 'when') return WhenWidget;
  if (name === 'option') return OptionsWidget;
  return Widgets[name] || Widgets.input;
};
