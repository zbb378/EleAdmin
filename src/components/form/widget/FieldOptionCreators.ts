import { Field, SelectorOption, Selector } from '../form';
import { ElCheckbox, ElOption, ElRadio, ElRadioButton } from 'element-plus';
import type { Component } from 'vue';
import { WidgetName } from '../widgets';

/**
 * 选择组件选项
 */
export interface OptionItem {
  tag: string | Component;
  label: string | number;
  value?: string | number;
  text?: string | number;
  disabled?: boolean;
}

/**
 * select|radio|checkbox options列表生成助手
 */
export type optionCreator = (item: SelectorOption, field?: Field) => OptionItem;

export const FieldOptionCreators: Record<Selector, optionCreator> = {
  select: (item) => ({
    tag: ElOption,
    label: item.label,
    value: item.value
  }),
  checkbox: (item) => ({
    tag: ElCheckbox,
    label: item.value,
    text: item.label
  }),
  radio: (item, field?: Field & { button?: boolean }) => ({
    tag: field?.button ? ElRadioButton : ElRadio,
    label: item.value,
    text: item.label
  })
};

export const getSelectorOptionItemCreator = (name: WidgetName) =>
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  FieldOptionCreators[name] ?? false;
