//  ====================== ElRow-ElCol ========================

import { computed, h, VNode } from 'vue';
import { ElButton, ElCol, ElDivider, ElRow } from 'element-plus';
import { Field } from '../form';
import {
  arrayCreator,
  coreCreator,
  FieldEvents,
  FormEvents,
  functional,
  getLayoutComponent,
  groupCreator,
  labelCreator,
  mainCreator,
  Wrapper,
  WrapperParam,
  YNode
} from './wrapper';
import { Row } from '../../elementUI';

export const rowLabelWrapper: Wrapper<FieldEvents> = ({ field }) => {
  if (!field.label) return [];
  return [
    h(
      ElRow,
      null,
      functional([
        h(
          ElCol,
          { span: 24 },
          functional(h(ElDivider, { contentPosition: 'left' }, functional(field.label)))
        )
      ])
    )
  ];
};

export const rowCoreWrapper: Wrapper<FieldEvents> = (param) => {
  const { field } = param;
  return [h(ElCol, { span: field?.col || 24 }, functional(coreCreator(param)))];
};

export const rowGroupWrapper: Wrapper<FieldEvents> = (param) => {
  const labelNode = labelCreator(param);

  const children = groupCreator(param);

  return [
    h(
      ElCol,
      { span: 24 },
      functional([
        h(ElRow, null, functional(h(ElCol, { span: 24 }, functional(labelNode)))),
        ...(children || [])
      ])
    )
  ];
};

export const rowMainWrapper: Wrapper<FormEvents, Field[]> = (param) => [
  h(ElRow, { style: { 'margin-bottom': '18px' } }, functional(mainCreator(param)))
];

export const btnStyle = {
  width: '100%'
};

export const addNodeProps = (fn?: () => void) => ({
  style: btnStyle,
  icon: 'el-icon-add',
  type: 'primary',
  onClick: fn,
  size: 'small'
});

export const addNode: (fn?: () => void) => VNode = (fn) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return h(ElButton, addNodeProps(fn), functional('+'));
};

export const deleteNodeProps = (fn?: () => void) => ({
  style: {
    ...btnStyle,
    margin: '0 10px 0 10px'
  },
  icon: 'el-icon-add',
  type: 'primary',
  onClick: fn,
  size: 'small'
});
export const deleteNode: (fn: () => void) => VNode = (fn) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return h(ElButton, deleteNodeProps(fn), functional('-'));
};

export const rowArrayCore: Wrapper<FieldEvents> = (param) => {
  const { handler, children } = arrayCreator(param);

  const { field } = param;

  let nodes: YNode[] =
    children?.map((node: unknown, index: number) =>
      h(
        ElRow,
        null,
        functional([
          h(ElCol, { span: 20 }, functional(h(ElRow, null, functional(node)))),
          h(ElCol, { span: 4 }, functional(deleteNode(() => handler?._array_remove(index))))
        ])
      )
    ) || [];

  addNode(handler?._array_add) &&
    nodes.push(
      h(ElRow, null, functional(h(ElCol, { span: 24 }, functional(addNode(handler?._array_add)))))
    );

  if (field.label) {
    const label = labelCreator(param);
    if (label) nodes = label.concat(nodes);
  }

  return nodes;
};

export const rowArrayWrapper: Wrapper<FieldEvents> = (param) => [
  h(ElCol, { span: 24, style: { 'margin-bottom': '18px' } }, functional(rowArrayCore(param)))
];

// noinspection JSUnusedGlobalSymbols
export default getLayoutComponent((props, { emit, slots, attrs }) => {
  const param = computed<WrapperParam<FormEvents, Field[]>>(() => {
    return {
      field: props.fields || [],
      value: props.modelValue as Row,
      events: {
        modelValue: (v: unknown) => emit('update:modelValue', v),
        elements: (v: Array<Field>) => emit('update:elements', v)
      },
      attrs,
      context: {
        slots,
        wrappers: {
          labelWrapper: rowLabelWrapper,
          groupWrapper: rowGroupWrapper,
          coreWrapper: rowCoreWrapper,
          arrayWrapper: rowArrayWrapper,
          mainWrapper: rowMainWrapper
        }
      }
    } as WrapperParam<FormEvents, Field[]>;
  });

  return rowMainWrapper(param.value);
});
