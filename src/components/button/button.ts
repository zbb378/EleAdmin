import { DisabledComponent, Er, Face, Row } from '../elementui';
import { CommandOptions, Sensor } from '../core';
import { ButtonName } from './buttons';

// noinspection JSUnusedGlobalSymbols
/**
 * 文件上传按钮参数定义
 */
export interface FileUpload extends Button {
  /**
   * 上传的地址
   */
  action: string;
  /**
   * 上传的文件字段名 --'file'
   */
  field?: string;
  /**
   * 是否支持多选文件 --false
   */
  multiple?: boolean;
  /**
   * 最大允许上传个数
   */
  limit?: number;
  /**
   * 是否显示已上传文件列表 --false
   */
  showFileList?: boolean;
  /**
   * 是否启用拖拽上传  --false
   */
  drag?: boolean;
  /**
   *  接受上传的文件类型 （thumbnail-mode 模式下此参数无效）
   */
  accept?: string;
  /**
   * 是否自动上传文件
   */
  autoUpload: boolean;
  /**
   * 列表样式 --'text'
   */
  listType: 'text' | 'picture' | 'picture-card';
  /**
   * 上传时附带的额外参数
   */
  data?: object;
  /**
   * 设置上传的请求头部
   */
  headers?: object;
  /**
   * 支持发送 cookie 凭证信息  --false
   */
  withCredentials?: boolean;
  /**
   * 缩略图模式
   */
  thumbnailMode?: boolean;
}

// noinspection JSUnusedGlobalSymbols
/**
 * 切换按钮参数定义 row[sensor] === values[i]
 */
export interface SwitchButton extends Button {
  icons?: string[] | string;
  labels?: string[] | string;
  faces?: Face[] | Face;
  sensor?: Sensor;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  values?: Array<any>;
  row?: Row;
}

/**
 * 弹出对话框定义
 */
export type Confirm = {
  message: string;
  title: string;
  options?: {
    distinguishCancelAndClose: boolean;
    confirmButtonText: string;
    cancelButtonText: string;
  };
};

export interface Button extends DisabledComponent, Er {
  /**
   * 类型
   */
  species?: ButtonName;

  /**
   * 按钮绑定的命令
   */
  command?: CommandOptions;

  /**
   * 外观颜色
   */
  face?: Face;
  /**
   * 是否朴素按钮
   */
  plain?: boolean;
  /**
   * 是否圆角按钮
   */
  round?: boolean;
  /**
   * 是否圆形按钮
   */
  circle?: boolean;

  /**
   * 弹出对话框
   */
  confirm?: Confirm;
}

export const buttons: Record<ButtonName, Button> = {
  common: {
    name: 'common',
    icon: 'icon-button',
    label: '普通'
  },
  dialog: {
    name: 'dialog',
    label: '对话框',
    icon: 'icon-dialog'
  },
  switch: {
    name: 'switch',
    label: '切换',
    icon: 'icon-switch'
  }
};

export type Toolbar = {
  items: Array<Button>;
  row?: Row;
};
