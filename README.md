# admin@Ep

感谢关注！

经过这段时间的完善，预想的功能基本上实现了。

## 概念

这套东西主要是基于element-plus实现的一个*语义化*的通用性**前端**,可用于实现零代码/低代码平台。

* 后端发送一个Json，前端自动生成表单、表格、图表、按钮、对话框
* 拥有Json生成器，可通过拖拽的方式自己设计

## 效果演示

语义化表格: 实现多种语义化单元格(超链接和随数据变化的彩色标签、彩色图标等)

![表格效果](https://images.gitee.com/uploads/images/2021/0309/120340_dd5d51d3_1472435.png "表格.png")

语义化表单: 新增关联域、表单域组，实现了表单域数组化

![表单](https://images.gitee.com/uploads/images/2021/0309/120424_d5e008fb_1472435.png "表单.png")

页面弹框: 基本上所有页面都可以以弹框形式出现

![对话框表单](https://images.gitee.com/uploads/images/2021/0309/120442_6e2a623a_1472435.png "对话框.png")

表单生成器、表格生成器、(Echart图标生成器)、模型生成器、模块生成器

![表单生成](public/%E8%A1%A8%E5%8D%95%E7%94%9F%E6%88%90.jpeg)
![表格生成](public/%E8%A1%A8%E6%A0%BC%E7%94%9F%E6%88%90.jpeg)
![模型生成](public/%E6%A8%A1%E5%9E%8B%E7%94%9F%E6%88%90.jpeg)
![模块管理](public/%E6%A8%A1%E5%9D%97%E7%AE%A1%E7%90%86.jpeg)

## Json实例

下面的Json将渲染一个复杂的表格：
```javascript
return {
      view: 'curd',
      definition: {
        title: '用户管理',
        url: '/index/table/rows',
        columns: [
          {
            name: 'name', label: '姓名', align: 'center', width: 200,
            display: {
              widget: 'url',
              relation: 'url'
            }
          }, {
            name: 'age', label: '年龄', align: 'center', width: 100, sortable: true
          }, {
            name: 'sex', label: '性别', align: 'center', width: 100,
            display: {
              widget: 'bool',
              texts: ['女', '男'],
              icons: ['el-icon-female', 'el-icon-male'],
              styles: [{
                color: '#F56C6C'
              }, {
                color: '#409EFF'
              }]
            }
          }, {
            name: 'address', label: '地址', align: 'left', 'header-align': 'center'
          }
        ],
        embedded: {
          items: [
            {
              type: 'success',
              label: '修改',
              redirect: {
                url: 'index/form/edit'
              },
              payload: true
            },{
              type: 'danger',
              label: '删除',
              payload: true,
              confirm: '确定要删除？'
            }
          ]
        },
        search: {
          options: [
            {
              label: '姓名',
              name: 'name'
            }, {
              label: '年龄',
              name: 'age'
            }
          ]
        },
        toolbar: {
          items: [
            {
              label: '新建',
              type: 'primary',
              dialog: true,
              title: '新增用户',
              url: 'index/form/add'
            }, {
              label: '删除',
              type: 'danger',
              diskey: 'selected',
              payload: true,
              url: '/index/api/delete'
            }
          ]
        }
      }
    }
```


## Vue 3 + TypeScript + Vite 实现
整个项目重新实现了一遍，更加健壮和清晰